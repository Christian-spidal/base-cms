<?php 

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

// Show debug messages
define ('DEBUG', true);

// Website URL and path
define('PATH', 'http://localhost/website/base/');
define('WEBSITE_TITLE', 'Example.com');

// Default controller to load — homepage requests are sent to this controller
define('DEFAULT_CONTROLLER', 'page');
define('DEFAULT_ACTION', 'index');

define('THEME', 'my_theme');