<?php

require("database.php");
require("config.php");
require("core.php");
require("router.php");
require("query_builder.php");

// Route the request
$router = new Router();

$router->route();