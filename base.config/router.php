<?php


class Router extends base_core {

	public function route() {
		// set the default controller
        $controller_name = DEFAULT_CONTROLLER;
        // set the default action
        $action_name = DEFAULT_ACTION;
        // explode the url into segements
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        // get the controller name from the segments
        if ( !empty($routes[3]) ) {   
            $controller_name = $routes[3];
        }
        // get name from the segments
        if ( !empty($routes[4]) ) {
            $action_name = $routes[4];
        }
        // check for stopwords for example base_admin redirects to the base admin area controller.
        if($controller_name == "base_admin") {
            // set path to base.admin folder
            $path = "base.admin";
            // set default controller to page
            $controller_name = "base_admin";
            //$action = 'index';
        } else if($controller_name == "error") {
            $path = "base.core";
            $controller_name = "page";
            $action_name = 'error';                  
        } else {
            // set path to frontend
            $path = "base.core";
            $controller_name = "page";
            $action_name = 'index';
        }    
        // set controller name
        $controller_name = $controller_name;
        // set action name
        $action_name = $action_name;
        // load file this controller class
        // strip the controller name to lower
        $controller_file = strtolower($controller_name).'.php';
        // write the path to the controller file
        $controller_path = ROOT . "/" . $path . "/controller/" . $controller_file;
        // check if the controller exists
        if(file_exists($controller_path))
        {
            // require the controller
            require ROOT . "/" . $path . "/controller/".$controller_file;
        }
        else
        {
            // redirect to 404 page
            Router::ErrorPage404();
        }
        
        $core = new base_core();

        $router_model = $core->model('router_model');
        // create controller
        $slugs = $router_model->check_slug($routes[3]);
        
        if( count($slugs) > 0 || $routes[3] == "" ) {
            $this->redirect($controller_name, $action_name, $routes);    
        } else if( $action_name === "error" ) {
            $this->redirect($controller_name, $action_name, $routes);
        } else if( $controller_name === "base_admin" ) {
            $this->redirect($controller_name, $action_name, $routes);   
        } else {
            // redirect to 404 page
            Router::ErrorPage404(); 
        }
	}

    function redirect($controller_name, $action, $routes) {
        define('CURRENT_CONTROLLER', $routes[3]);
        //define('CURRENT_ACTION', $routes[4]);
        $controller = new $controller_name;
        if(method_exists($controller, $action)) {
            // call action of controller
            $controller->$action();
        } else {
            // redirect to 404 page
            Router::ErrorPage404();   
        }
    }

	function ErrorPage404()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/website/base/error/404';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host);
    }
	
}