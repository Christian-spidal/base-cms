<?php

/**
 * This is the "base controller class". All other "real" controllers extend this class.
 */
class base_core {

	/**
     * @var null Database Connection
     */
    public $db = null;
    protected $data = null;

	function __construct() {
		$this->initDatabaseConnection();
	}

	/**
     * Setup a database connection
     * This will use the setup details which will be published within config.php
     */
	private function initDatabaseConnection()
    {
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS, $options);
    }

	public function base_header($data = false) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/themes/" . THEME . "/header.php") ) {
			require (ROOT . "/themes/" . THEME . "/header.php");
		} else {
			/* throw exception */						
		}
	}

	public function base_footer($data = false) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/themes/" . THEME . "/footer.php") ) {
			require (ROOT . "/themes/" . THEME . "/footer.php");
		} else {
			/* throw exception */						
		}
	}

	public function model($model_name) {
		require ROOT . '/base.core/model/' . strtolower($model_name) . '.php';
        // return new model (and pass the database connection to the model)
        return new $model_name($this->db);
	}

	public function admin_model($model_name) {
		require ROOT . '/base.admin/model/' . strtolower($model_name) . '.php';
        // return new model (and pass the database connection to the model)
        return new $model_name($this->db);
	}

	public function view($view_name, $data) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/themes/" . THEME . "/" . $view_name . '.php') ) {
			include (ROOT . "/themes/" . THEME . "/" . $view_name . '.php');
		} else {
			/* throw exception */
			die("File not found at " . ROOT . "/themes/" . THEME . "/" . $view_name . ".php");	
		}
	}

	public function globals() {
		//$settings_model = $this->model('settings_model');
		$settings['home_url'] = PATH;
		$settings['settings'] = array("background" => "#CCC");
		$settings['users'] = array("username" => "Chris");
		return $settings;
	}

	/* admin details */

	public function admin_header($data = false) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/base.admin/views/header.php") ) {
			require (ROOT . "/base.admin/views/header.php");
		} else {
			/* throw exception */
			die("File not found at " . ROOT . "/base.admin/views/header.php");					
		}
	}

	public function admin_footer($data = false) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/base.admin/views/footer.php") ) {
			require (ROOT . "/base.admin/views/footer.php");
		} else {
			/* throw exception */
			die("File not found at " . ROOT . "/base.admin/views/footer.php");					
		}
	}

	public function admin_sidebar($data = false) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/base.admin/views/sidebar.php") ) {
			require (ROOT . "/base.admin/views/sidebar.php");
		} else {
			/* throw exception */
			die("File not found at " . ROOT . "/base.admin/views/sidebar.php");					
		}
	}

	public function admin_view($view_name, $data = false) {
		if($data != "" && $data != false) {
			extract($data);
		}
		extract($this->globals());
		if( file_exists(ROOT . "/base.admin/views/" . $view_name . '.php') ) {
			// Require the file
			ob_start();
			require(ROOT . "/base.admin/views/" . $view_name . '.php');
			// Return the string
			$strView = ob_get_contents();
			ob_end_clean();
			return $strView;
		} else {
			/* throw exception */
			die("File not found at " . ROOT . "/base.admin/views/" . $view_name . '.php');	
		}
	}
}