<?php 

class query extends base_core {

	var $select;
	var $from;
	var $where;
	var $equals;

	function select($select) {
		$this->select = $select;
	}

	function from($select) {
		$this->from = $select;
	}

	function where($_this, $_equals) {
		$this->where = $_this;
		$this->equals = $_equals;
	}
	
	function execute() {
		return "SELECT " . $this->select . " FROM " . $this->from . " WHERE " . $this->where . " " . $this->equals;
	}

}