<?php

/**
 * Base CMS written in PHP based on MVC.
 *
 * @package Base CMS
 * @author Christian Burd
 * @link http://www.christianburd.co.uk/base
 * @link https://github.com/spidal/base
 * @license http://opensource.org/licenses/MIT MIT License
 */

if (file_exists('base.config/autoload.php')) {
    require 'base.config/autoload.php';
}