	</div>
	<!-- jQuery (necessary for Flat UI's JavaScript plugins) -->
    <script src="<?php echo $home_url; ?>base.admin/includes/js/vendor/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $home_url; ?>base.admin/includes/js/vendor/video.js"></script>
    <script src="<?php echo $home_url; ?>base.admin/includes/js/flat-ui.min.js"></script>
    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>
  </body>
</html>