<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <title>Base</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	    <!-- Loading Bootstrap -->
	    <link href="<?php echo $home_url; ?>base.admin/includes/css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <!-- Loading Flat UI -->
	    <link href="<?php echo $home_url; ?>base.admin/includes/css/flat-ui.min.css" rel="stylesheet">
	    <!-- Base UI -->
	    <link href="<?php echo $home_url; ?>base.admin/includes/css/base_css.css" rel="stylesheet">
	</head>
<body>
<header>
	<div class="container-fluid">
    <div class="row">
	<div class="col-xs-12 no-gutter">
	  <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
	        <span class="sr-only">Toggle navigation</span>
	      </button>
	      <a class="navbar-brand" href="#">Base</a>
	    </div>
	    <div class="collapse navbar-collapse" id="navbar-collapse-01">
	      <ul class="nav navbar-nav navbar-left">
	        <li><a href="#fakelink">Menu Item<span class="navbar-unread">1</span></a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Messages <b class="caret"></b></a>
	          <span class="dropdown-arrow"></span>
	          <ul class="dropdown-menu">
	            <li><a href="#">Action</a></li>
	            <li><a href="#">Another action</a></li>
	            <li><a href="#">Something else here</a></li>
	            <li class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	          </ul>
	        </li>
	        <li><a href="#fakelink">About Us</a></li>
	       </ul>
	       <form class="navbar-form navbar-right" action="#" role="search">
	        <div class="form-group">
	          <div class="input-group">
	            <input class="form-control" id="navbarInput-01" type="search" placeholder="Search">
	            <span class="input-group-btn">
	              <button type="submit" class="btn"><span class="fui-search"></span></button>
	            </span>
	          </div>
	        </div>
	      </form>
	    </div><!-- /.navbar-collapse -->
	  </nav><!-- /navbar -->
	</div>
	</div>
	</div>
</header>
<div id="wrapper" class="row">