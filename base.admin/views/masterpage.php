<?php $this->admin_header($data); ?>

<?php $this->admin_sidebar($data); ?>

<div id="page-content-wrapper">
	<div class="container-fluid">
		<a href="#menu-toggle" class="fui-list" id="menu-toggle"></a>
    	<?php echo $main_content; ?>
    </div>
</div>

<?php $this->admin_footer($data); ?>