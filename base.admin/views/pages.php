<h3>All pages <a class="btn palette-turquoise" href="">Add Pages</a></h3>

<input type="text" value="" placeholder="Search" class="form-control">

<table width="100%" class="table">
	<thead>
		<tr>
			<th width="40px">
				<label class="checkbox" for="checkbox1">
	            	<input type="checkbox" value="" id="checkbox1" data-toggle="checkbox" class="custom-checkbox"><span class="icons"><span class="icon-unchecked"></span><span class="icon-checked"></span></span>            
	          	</label>
	        </th>
			<th>Page Title</th>
			<th>Author</th>
			<th>Published</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($pages as $page) { ?>
		<tr>
			<td>
				<label class="checkbox" for="checkbox1">
	            	<input type="checkbox" value="" id="checkbox1" data-toggle="checkbox" class="custom-checkbox"><span class="icons"><span class="icon-unchecked"></span><span class="icon-checked"></span></span>            
	          	</label>
  			</td>
			<td><a href=""><?php echo $page['page_name']; ?></a></td>
			<td><a href="">Chris</a></td>
			<td><p><?php echo $page['date']; ?></p></td>
		</tr>
		<?php } ?>
	</tbody>
</table>

<?php var_dump($pages); ?>