<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="#">
                Start Bootstrap
            </a>
        </li>
        <li>
            <a href="<?php echo $home_url; ?>base_admin/"><span class="fui-home"></span> Dashboard</a>
        </li>
        <li>
            <a href="<?php echo $home_url; ?>base_admin/pages"><span class="fui-list"></span> Pages</a>
        </li>
        
    </ul>
</div>
<!-- /#sidebar-wrapper -->