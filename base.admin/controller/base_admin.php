<?php 

class base_admin extends base_core {

	function index() {
		$data['stats'] = "";		
		$data['main_content'] = $this->admin_view("dashboard", $data);
		echo $this->admin_view("masterpage", $data);
	}

	function pages() {
		$admin_pages_model = $this->admin_model("admin_pages_model");
		$data['pages'] = $admin_pages_model->get_all_pages();		
		$data['main_content'] = $this->admin_view("pages", $data);
		echo $this->admin_view("masterpage", $data);
	}

	function new_page() {	
		if(isset($_POST['new_page'])) {
			$admin_pages_model = $this->admin_model("admin_pages_model");
			$data['pages'] = $admin_pages_model->insert_page();
			echo "added";
		}	
		$data['main_content'] = $this->admin_view("new_page");
		echo $this->admin_view("masterpage", $data);	
	}

	function edit_page() {
		$data['main_content'] = $this->admin_view("new_page");
		echo $this->admin_view("masterpage", $data);	
	}

	function users() {

		/*$query = new query();
		$query->select("*");
		$query->from("table");
		$query->where("xxxx", "ssss");
		echo $query->execute();*/

		$data['main_content'] = "xxx";
		echo $this->admin_view("masterpage", $data);
	}

	function error() {
		echo "404";
	}

}