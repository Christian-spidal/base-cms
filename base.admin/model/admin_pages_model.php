<?php

class admin_pages_model
{
    /**
     * Every model needs a database connection, passed to the model
     * @param object $db A PDO database connection
     */
    function __construct($db) {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    function check_slug($slug)
    {
        $sql = "SELECT slug FROM base_pages WHERE slug = '" . $slug . "'";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    function get_all_pages() {
        $sql = "SELECT * FROM base_pages";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();   
    }

    function insert_page() {
        $slug = str_replace(' ','-', $_POST['page_title']);
        $page_title = $_POST['page_title'];
        $page_content = $_POST['page_content'];
        $sql = "INSERT INTO base_pages ( slug, page_name, content ) VALUES ( :slug, :page_title, :content )";
        $query = $this->db->prepare($sql);
        //$stmt = $db->prepare('INSERT INTO Persons (FirstName, LastName, Age) VALUES (:first_name, :last_name, :age)');

        $query->execute(array(':slug' => $slug,':page_title' => $page_title, ':content' => $page_content));

        echo "<h1>Query executed</h1>";

        //$query = $this->db->prepare($sql);
        //$query->execute();
    }
}
