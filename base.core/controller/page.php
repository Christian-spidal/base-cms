<?php 

class page extends base_core {

	function index() {
		$page_model = $this->model("pages_model");
		$data['page'] = $page_model->get_page_meta();		
		$this->view("homepage", $data);
	}

	function error() {
		echo "404";
	}

}